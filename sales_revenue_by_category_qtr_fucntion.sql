-- Drop function if it already exists
DROP FUNCTION IF EXISTS get_sales_revenue_by_category_qtr(targetQuarter INT);

-- Create new function
CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(targetQuarter INT)
RETURNS TABLE (
    category_id SMALLINT,
    category_name VARCHAR(255),
    total_sales_revenue DECIMAL(10, 2)
) AS $$
BEGIN
    RETURN QUERY
    SELECT
        fc.category_id::SMALLINT,
        c.name::VARCHAR AS category_name,
        SUM(p.amount) AS total_sales_revenue
    FROM
        rental r
        JOIN payment p ON r.rental_id = p.rental_id
        JOIN inventory i ON r.inventory_id = i.inventory_id
        JOIN film f ON i.film_id = f.film_id
        JOIN film_category fc ON f.film_id = fc.film_id
        JOIN category c ON fc.category_id = c.category_id
    WHERE
        EXTRACT(YEAR FROM p.payment_date) = 2017
        AND EXTRACT(QUARTER FROM p.payment_date) = targetQuarter
    GROUP BY
        fc.category_id, c.name
    HAVING
        SUM(p.amount) > 0;
END;
$$ LANGUAGE plpgsql;


select * from get_sales_revenue_by_category_qtr(1)