--Drop the view if it already exists
DROP FUNCTION IF EXISTS new_movie(title_param VARCHAR);

--Create new View
CREATE VIEW sales_revenue_by_category_qtr AS
SELECT
    fc.category_id,
    c.name AS category_name,
    SUM(p.amount) AS total_sales_revenue
FROM
    rental r
    JOIN payment p ON r.rental_id = p.rental_id
    JOIN inventory i ON r.inventory_id = i.inventory_id
    JOIN film f ON i.film_id = f.film_id
    JOIN film_category fc ON f.film_id = fc.film_id
    JOIN category c ON fc.category_id = c.category_id
WHERE
    EXTRACT(YEAR FROM p.payment_date) = 2017
    AND EXTRACT(QUARTER FROM p.payment_date) = 1
GROUP BY
    fc.category_id, c.name
HAVING
     SUM(p.amount) > 0;

select * from sales_revenue_by_category_qtr;
